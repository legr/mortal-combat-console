<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 21.04.18
 * Time: 23:24
 */

namespace Combat\Domain\Combatant\Combatants;


use Combat\Domain\Combatant\Combatant;
use Combat\Domain\Combatant\Skills\LuckyStrike;

class Swordsman extends Combatant
{

    function allowedValues(): array
    {
        return [
            'health' => [40, 60],
            'strength' => [60, 70],
            'defense' => [20, 30],
            'speed' => [90, 100],
            'luck' => [0.3, 0.5],
        ];
    }
    protected function configure()
    {
        $this->skills[] = new LuckyStrike();
    }

}