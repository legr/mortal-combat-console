<?php
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 23:53
 */

namespace Combat\Domain\ObjectValues;

use PHPUnit\Framework\TestCase;

class RangeObjectValueTest extends TestCase
{

    public function testSetValue()
    {
        $obj = new RangeObjectValue(200);
        $this->assertEquals(100, $obj->value());
        $obj = new RangeObjectValue(0);
        $this->assertEquals(0, $obj->value());

        $obj = new RangeObjectValue(50);
        $this->assertEquals(50, $obj->value());
    }
}
