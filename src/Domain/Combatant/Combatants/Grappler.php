<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 21.04.18
 * Time: 23:24
 */

namespace Combat\Domain\Combatant\Combatants;


use Combat\Domain\Combatant\Combatant;
use Combat\Domain\Combatant\Skills\CounterAttack;

class Grappler extends Combatant
{
    function allowedValues(): array
    {
        return [
            'health' => [60, 100],
            'strength' => [75, 80],
            'defense' => [35, 40],
            'speed' => [60, 80],
            'luck' => [0.3, 0.4],
        ];
    }

    protected function configure()
    {
        $this->skills[] = new CounterAttack();
    }

}