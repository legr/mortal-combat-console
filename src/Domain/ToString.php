<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * User: oc
 * Date: 15.10.17
 * Time: 19:06
 */

namespace Combat\Domain;


trait ToString
{
    public function __toString()
    {
        $result = [];
        $reflection = new \ReflectionClass($this);
        foreach ($reflection->getProperties() as $prop) {
            $field = $prop->getName();
            if ($this->includeToString() && !in_array($field, $this->includeToString())) {
                continue;
            }
            if (in_array($field, $this->excludeToString())) {
                continue;
            }
            $value = $this->$field;
            if (!is_null($value)) {
                if (is_array($value)) {
                    $result[] = $field . ':' . implode('|', $value);
                } else {
                    if (is_bool($value)) {
                        $value = $value ? 'yes' : 'no';
                    }
                    $result[] = $field . ':' . (string)$value;
                }
            }
        }

        return '(' . $this->className() . '){' . implode('; ', $result) . '}';
    }

    protected function includeToString()
    {
        return [];
    }

    protected function excludeToString()
    {
        return [];
    }

    public function className(): string
    {
        $className = get_class($this);

        if ($pos = strripos($className, '\\')) {
            return substr($className, $pos + 1);
        }

        return $className;

    }
}