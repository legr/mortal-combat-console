<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 21.04.18
 * Time: 23:24
 */

namespace Combat\Domain\Combatant\Combatants;


use Combat\Domain\Combatant\Combatant;
use Combat\Domain\Combatant\Skills\StunningBlow;

class Brute extends Combatant
{
    protected function allowedValues(): array
    {
        return [
            'health' => [90, 100],
            'strength' => [65, 75],
            'defense' => [40, 50],
            'speed' => [40, 65],
            'luck' => [0.3, 0.35],
        ];
    }

    protected function configure()
    {
        $this->skills[] = new StunningBlow();
    }


}