<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 12:05
 */

namespace Combat\Domain;


class ObjectValue
{
    protected $value;

    public function __construct($value)
    {
        $this->setValue($value);
    }

    public function inc($value)
    {
        return new static($this->value + $value);
    }


    public function decr($value)
    {
        return new static($this->value - $value);
    }

    public function multiply($value)
    {
        return new static($this->value * $value);
    }


    public function divide($value)
    {
        return new static(intval($this->value / $value));
    }


    public function value()
    {
        return $this->value;
    }

    public function setValue($value): void
    {
        $this->value = $value;
    }

    public function __toString()
    {
        return (string)$this->value;
    }

}