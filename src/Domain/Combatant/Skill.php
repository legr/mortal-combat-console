<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 19:21
 */

namespace Combat\Domain\Combatant;


use Combat\Domain\LoggerTrait;

abstract class Skill
{
    use LoggerTrait;

    abstract public function execute(Combatant $owner, Combatant $opponent);

}