<?php
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 22:35
 */

namespace Combat\Domain;

use PHPUnit\Framework\TestCase;

class RandomizerTest extends TestCase
{
    public function testsRand100()
    {
        $randomizer = new Randomizer();
        $result = $randomizer->rand100(100);
        $this->assertTrue($result);

        $result = $randomizer->rand100(0);
        $this->assertFalse($result);
    }

}
