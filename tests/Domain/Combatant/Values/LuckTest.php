<?php
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 21:43
 */

namespace Combat\Domain\Combatant\Values;

use Combat\Domain\Combatant\Values\LuckTest\LuckMock;
use PHPUnit\Framework\TestCase;

class LuckTest extends TestCase
{
    public function testIsRandInInterval()
    {

        $luck = new LuckMock(0.4);
        $this->assertFalse($luck->isRandInInterval());

        $luck = new LuckMock(0.5);
        $this->assertTrue($luck->isRandInInterval());

        $luck = new LuckMock(0.6);
        $this->assertTrue($luck->isRandInInterval());
    }
}

namespace Combat\Domain\Combatant\Values\LuckTest;

use Combat\Domain\Combatant\Values\Luck;

class LuckMock extends Luck
{
    protected function randLucky()
    {
        return 0.5;
    }
}