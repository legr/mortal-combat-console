<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 19:23
 */

namespace Combat\Domain\Combatant\Skills;


use Combat\Domain\Combatant\Combatant;
use Combat\Domain\Combatant\Skill;

class CounterAttack extends Skill
{
    const Damage = 10;

    public function execute(Combatant $owner, Combatant $opponent)
    {
        if ($owner->isDefender() && $owner->hasLuckyDefense()) {
            $this->log('#-><<' . $owner->name() . ' held a counterattack, and ' . $opponent->name() . ' is dealt 10 damage');
            $opponent->decrHealth(self::Damage);
        }
    }
}