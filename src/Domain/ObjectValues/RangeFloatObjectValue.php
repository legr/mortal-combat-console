<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 12:05
 */

namespace Combat\Domain\ObjectValues;


class RangeFloatObjectValue extends RangeObjectValue
{
    protected $range = [0.0, 1.0];

    protected static function rand($min, $max)
    {
        $rand = $min + mt_rand() / mt_getrandmax() * ($max - $min);
        $rand = round($rand, 1);
        return $rand;
    }

}