<?php
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 22:58
 */

namespace Combat\Domain\Combatant\Skills;

use Combat\Domain\Combatant\Skills\StunningBlowTest\CombMock;
use Combat\Domain\Combatant\Skills\StunningBlowTest\StunningBlowMock;
use PHPUnit\Framework\TestCase;

class StunningBlowTest extends TestCase
{

    public function testExecuteShouldOpponentMissingNextAttack()
    {
        $skill = new StunningBlowMock();
        $comb1 = new CombMock();
        $comb2 = new CombMock();

        $comb2->setHasLuckyDefense(false);
        $skill->execute($comb1, $comb2);
        $this->assertTrue($comb2->isMissingNextAttack());

    }

    public function testExecuteShouldNotOpponentMissingNextAttackIfDefended()
    {
        $skill = new StunningBlowMock();
        $comb1 = new CombMock();
        $comb2 = new CombMock();

        $comb2->setHasLuckyDefense(true);
        $skill->execute($comb1, $comb2);
        $this->assertFalse($comb2->isMissingNextAttack());

    }
}

namespace Combat\Domain\Combatant\Skills\StunningBlowTest;

use Combat\Domain\Combatant\Combatant;
use Combat\Domain\Combatant\Skills\StunningBlow;

class StunningBlowMock extends StunningBlow
{
    protected function chance()
    {
        return true;
    }
}

class CombMock extends Combatant
{
    protected $name = 'Acme';

    /**
     * CombMock constructor.
     */
    public function __construct()
    {
    }

    public function setHasLuckyDefense($hasLuckyDefense)
    {
        $this->hasLuckyDefense = $hasLuckyDefense;
    }

    protected function allowedValues(): array
    {

    }
}

