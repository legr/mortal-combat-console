<?php
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 20:39
 */

namespace Combat\Domain;

use Combat\Domain\BattleTest\BattleMock;
use Combat\Domain\Combatant\Values\Luck;
use PHPUnit\Framework\TestCase;

class BattleTest extends TestCase
{

    public function testChoseAttacker()
    {
        $battle = $this->battle();
        $battle->choseAttacker();
        $this->assertTrue($battle->comb1()->isAttacker());
        $this->assertTrue($battle->comb2()->isDefender());

        $battle->choseAttacker();
        $this->assertTrue($battle->comb1()->isDefender());
        $this->assertTrue($battle->comb2()->isAttacker());

    }

    private function battle()
    {
        return new BattleMock();
    }

    public function testChoseAttackerIfMissingNextAttack()
    {
        $battle = $this->battle();
        $battle->choseAttacker();
        $this->assertTrue($battle->comb1()->isAttacker());
        $this->assertTrue($battle->comb2()->isDefender());

        $battle->comb2()->setIsMissingNextAttack(true);
        $battle->choseAttacker();
        $this->assertTrue($battle->comb1()->isAttacker());
        $this->assertTrue($battle->comb2()->isDefender());

    }

    public function testCalcLuck()
    {
        $battle = $this->battle();
        $comb = $battle->comb2();

        $comb->setLuck(new Luck(1.0))->setIsDefender();
        $battle->calcLuck();
        $this->assertTrue($comb->hasLuckyDefense());

        $comb->setLuck(new Luck(0.0))->setIsDefender();
        $battle->calcLuck();
        $this->assertFalse($comb->hasLuckyDefense());

    }


}

namespace Combat\Domain\BattleTest;

use Combat\Domain\Battle;
use Combat\Domain\Combatant\Combatant;
use Combat\Domain\Combatant\Combatants\Brute;
use Combat\Domain\Combatant\Combatants\Grappler;
use Combat\Domain\Combatant\Values\Luck;

class BattleMock extends Battle
{

    public function __construct()
    {
        $this->comb1 = new BruteSpy('Foo');
        $this->comb2 = new GrapplerSpy('Bar');
    }

    public function calcLuck()
    {
        parent::calcLuck();
    }

    public function choseAttacker()
    {
        parent::choseAttacker();
    }

    public function comb1(): BruteSpy
    {
        return $this->comb1;
    }

    public function comb2(): GrapplerSpy
    {
        return $this->comb2;
    }


}

class BruteSpy extends Brute
{
    public function setLuck(Luck $luck): Combatant
    {
        return parent::setLuck($luck);
    }
}

class GrapplerSpy extends Grappler
{
    public function setLuck(Luck $luck): Combatant
    {
        return parent::setLuck($luck);
    }
}