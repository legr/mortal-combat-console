<?php
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 23:15
 */

namespace Combat\Domain\Combatant\Skills;

use Combat\Domain\Combatant\Skills\CounterAttackTest\CombMock;
use Combat\Domain\Combatant\Skills\CounterAttackTest\CounterAttackMock;
use PHPUnit\Framework\TestCase;

class CounterAttackTest extends TestCase
{

    public function testExecute()
    {
        $skill = new CounterAttackMock();
        $comb1 = new CombMock();
        $comb2 = new CombMock();

        $comb1->setIsDefender(true);
        $comb1->setHasLuckyDefense(true);
        $skill->execute($comb1, $comb2);
        $this->assertEquals(40, $comb2->health()->value());

    }

    public function testExecute2()
    {
        $skill = new CounterAttackMock();
        $comb1 = new CombMock();
        $comb2 = new CombMock();

        $comb1->setHasLuckyDefense(false);
        $skill->execute($comb1, $comb2);
        $this->assertEquals(50, $comb2->health()->value());
    }
}


namespace Combat\Domain\Combatant\Skills\CounterAttackTest;

use Combat\Domain\Combatant\Combatant;
use Combat\Domain\Combatant\Skills\CounterAttack;
use Combat\Domain\Combatant\Values\Health;

class CounterAttackMock extends CounterAttack
{

    protected function chance()
    {
        return true;
    }
}

class CombMock extends Combatant
{
    protected $name = 'Acme';
    /**
     * CombMock constructor.
     */
    public function __construct()
    {
        $this->health = new Health(50);
    }

    public function setHasLuckyDefense($hasLuckyDefense)
    {
        $this->hasLuckyDefense = $hasLuckyDefense;
    }

    protected function allowedValues(): array
    {

    }
}
