<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 12:05
 */

namespace Combat\Domain\ObjectValues;


use Combat\Domain\Exceptions\InvalidRangeObjectValueException;
use Combat\Domain\Exceptions\NotNumericRangeObjectValueException;
use Combat\Domain\ObjectValue;

class RangeObjectValue extends ObjectValue
{

    protected $range = [0, 100];

    public static function instanceByRange($min, $max)
    {
        if (!is_numeric($min) || !is_numeric($max)) {
            throw new NotNumericRangeObjectValueException(sprintf('min:%s, max:%s', $min, $max));
        }
        if ($min > $max) {
            throw new InvalidRangeObjectValueException(sprintf('min:%s, max:%s', $min, $max));
        }

        $value = static::rand($min, $max);

        return new static($value);
    }

    protected static function rand($min, $max)
    {
        return mt_rand($min, $max);
    }

    public function setValue($value): void
    {
        list($min, $max) = $this->range;

        if ($value < $min) {
            $value = $min;
        }
        if ($value > $max) {
            $value = $max;
        }
        $this->value = $value;
    }

    public function isZero()
    {
        return $this->value == 0;
    }
}