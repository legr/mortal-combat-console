<?php declare(strict_types=1);

require __DIR__.'/vendor/autoload.php';

use Combat\Command\RunCommand;
use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new RunCommand());

$application->run();