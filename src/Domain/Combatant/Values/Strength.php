<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 12:05
 */

namespace Combat\Domain\Combatant\Values;


use Combat\Domain\ObjectValues\RangeObjectValue;

class Strength extends RangeObjectValue
{

}