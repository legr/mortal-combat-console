<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 22:30
 */

namespace Combat\Domain;


class Randomizer
{
    public function rand100($max)
    {
        $rand = mt_rand(1, 99);
        if ($rand <= $max) {
            return true;
        }
        return false;
    }
}