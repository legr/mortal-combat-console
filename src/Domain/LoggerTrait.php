<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 23.04.18
 * Time: 10:21
 */

namespace Combat\Domain;


trait LoggerTrait
{
    /**
     * @var Logger
     */
    protected $logger;

    protected function log($message)
    {
        if (!$this->logger) {
            $this->logger = Logger::instance();
        }
        $this->logger->log($message);
    }
}