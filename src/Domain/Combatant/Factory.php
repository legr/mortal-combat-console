<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 21.04.18
 * Time: 23:20
 */

namespace Combat\Domain\Combatant;


use Combat\Domain\Combatant\Combatants\Brute;
use Combat\Domain\Combatant\Combatants\Grappler;
use Combat\Domain\Combatant\Combatants\Swordsman;

class Factory
{
    protected $combatants = [
        Brute::class,
        Grappler::class,
        Swordsman::class,
    ];

    public function randomWithName(string $name): Combatant
    {
        $combatants = $this->combatants;
        $index = rand(0, count($combatants) - 1);
        $class = $combatants[$index];
        return new $class($name);
    }
}