<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 23.04.18
 * Time: 0:13
 */

namespace Combat\Domain;


class Logger
{
    static $instance;
    static protected $messages = [];

    /**
     * Logger constructor.
     */
    private function __construct()
    {
    }

    public static function instance()
    {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @param string|string[] $message
     */
    public static function log($message)
    {
        $messages = is_array($message) ? $message : [$message];
        foreach ($messages as $message) {
            self::$messages[] = (string)$message;
        }
    }

    /**
     * @return array
     */
    public static function messages(): array
    {
        return self::$messages;
    }


}