<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 19:23
 */

namespace Combat\Domain\Combatant\Skills;


use Combat\Domain\Combatant\Combatant;
use Combat\Domain\Combatant\Skill;
use Combat\Domain\Randomizer;

class StunningBlow extends Skill
{
    const Chance = 2;

    /**
     * @var Randomizer
     */
    protected $randomizer;

    /**
     * LuckyStrike constructor.
     */
    public function __construct()
    {
        $this->randomizer = new Randomizer();
    }

    public function execute(Combatant $owner, Combatant $opponent)
    {
        if ($owner->isAttacker() && !$opponent->hasLuckyDefense() && $this->chance()) {
            $opponent->setIsMissingNextAttack();
            $this->log('&->>>' . $owner->name() . 'stunning the ' . $opponent->name() . ',  he will miss his next attack');

        }

    }

    protected function chance()
    {
        return $this->randomizer->rand100(self::Chance);
    }
}