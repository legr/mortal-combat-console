<?php
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 23:55
 */

namespace Combat\Domain\ObjectValues;

use PHPUnit\Framework\TestCase;

class RangeFloatObjectValueTest extends TestCase
{
    public function testSetValue()
    {
        $obj = new RangeFloatObjectValue(1);
        $this->assertEquals(1, $obj->value());

        $obj = new RangeFloatObjectValue(0);
        $this->assertEquals(0, $obj->value());

        $obj = new RangeFloatObjectValue(0.5);
        $this->assertEquals(0.5, $obj->value());
    }
}
