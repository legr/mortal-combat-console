<?php
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 22:04
 */

namespace Combat\Domain\Combatant\Skills;

use Combat\Domain\Combatant\Skills\LuckStrikeTest\CombMock;
use Combat\Domain\Combatant\Skills\LuckStrikeTest\LuckyStrikeMock;
use PHPUnit\Framework\TestCase;

class LuckyStrikeTest extends TestCase
{

    public function testExecuteShouldIncreaseStrength()
    {
        $skill = new LuckyStrikeMock();
        $comb1 = new CombMock();
        $comb2 = new CombMock();

        $comb2->setHasLuckyDefense(false);
        $skill->execute($comb1, $comb2);
        $this->assertEquals(20, $comb1->strength()->value());

    }

    public function testExecuteShouldNotIncreaseIfOpponentDefended()
    {
        $skill = new LuckyStrikeMock();
        $comb1 = new CombMock();
        $comb2 = new CombMock();


        $comb2->setHasLuckyDefense(true);
        $skill->execute($comb1, $comb2);
        $this->assertEquals(10, $comb1->strength()->value());
    }
}

namespace Combat\Domain\Combatant\Skills\LuckStrikeTest;

use Combat\Domain\Combatant\Combatant;
use Combat\Domain\Combatant\Skills\LuckyStrike;
use Combat\Domain\Combatant\Values\Strength;

class LuckyStrikeMock extends LuckyStrike
{
    protected function chance()
    {
        return true;
    }
}

class CombMock extends Combatant
{
    protected $name = 'Acme';

    /**
     * CombMock constructor.
     */
    public function __construct()
    {
        $this->strength = new Strength(10);
    }

    public function setHasLuckyDefense($hasLuckyDefense)
    {
        $this->hasLuckyDefense = $hasLuckyDefense;
    }

    protected function allowedValues(): array
    {

    }
}

