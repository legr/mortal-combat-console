<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 0:01
 */

namespace Combat\Domain\Exceptions;


class NotNumericRangeObjectValueException extends \Exception
{

}