<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 12:05
 */

namespace Combat\Domain\Combatant\Values;


use Combat\Domain\ObjectValues\RangeFloatObjectValue;

class Luck extends RangeFloatObjectValue
{

    public function isRandInInterval()
    {
        $rand = $this->randLucky();
        if ($rand <= $this->value) {
            return true;
        }
        return false;
    }

    protected function randLucky()
    {
        $rand = mt_rand(0, 10);
        return $rand / 10;
    }
}