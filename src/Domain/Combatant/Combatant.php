<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 21.04.18
 * Time: 23:18
 */

namespace Combat\Domain\Combatant;


use Combat\Domain\Combatant\Values\Defense;
use Combat\Domain\Combatant\Values\Health;
use Combat\Domain\Combatant\Values\Luck;
use Combat\Domain\Combatant\Values\Speed;
use Combat\Domain\Combatant\Values\Strength;
use Combat\Domain\Logger;
use Combat\Domain\ObjectValues\RangeObjectValue;
use Combat\Domain\ToString;

abstract class Combatant
{
    use ToString;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var Health
     */
    protected $health;

    /**
     * @var Strength
     */
    protected $strength;

    /**
     * @var Defense
     */
    protected $defense;

    /**
     * @var Speed
     */
    protected $speed;

    /**
     * @var Luck
     */
    protected $luck;

    protected $skills = [];

    /**
     * @var boolean
     */
    protected $isDefender = false;

    /**
     * @var boolean
     */
    protected $hasLuckyDefense = false;

    protected $isMissingNextAttack = false;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * Combatant constructor.
     * @param $name
     */
    public function __construct($name)
    {
        $this->setName($name);
        $this->setValues();
        $this->configure();
    }

    protected function setValues()
    {
        foreach ($this->allowedValues() as $field => list($min, $max)) {
            $setter = 'set' . ucfirst($field);
            $value = $this->createValue($field, $min, $max);
            $this->$setter($value);
        }
    }

    protected function configure()
    {

    }

    abstract protected function allowedValues(): array;


    protected function createValue($name, $min, $max): RangeObjectValue
    {
        $class = $this->getObjectClass($name);
        $object = $class::instanceByRange($min, $max);
        return $object;
    }

    /**
     * @param $name
     * @return string|RangeObjectValue
     */
    protected function getObjectClass($name)
    {
        return 'Combat\Domain\Combatant\Values\\' . ucfirst($name);
    }

    public function scoreboard()
    {
        return $this->className() . ' ' . $this->name . '| Health:' . $this->health;
    }

    public function damage(Strength $strength)
    {
        $damage = $strength->value() - $this->defense->value();
        $this->decrHealth($damage);
        return $damage;
    }

    public function decrHealth($value)
    {
        $this->health = $this->health->decr($value);
    }

    /**
     * @return string
     */
    public function name(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    protected function setName(string $name): void
    {
        $this->name = mb_substr($name, 0, 30);
    }


    /**
     * @return Health
     */
    public function health(): Health
    {
        return $this->health;
    }

    /**
     * @param Health $health
     * @return Combatant
     */
    protected function setHealth(Health $health): Combatant
    {
        $this->health = $health;
        return $this;
    }

    /**
     * @return Strength
     */
    public function strength(): Strength
    {
        return $this->strength;
    }

    /**
     * @param Strength $strength
     * @return Combatant
     */
    protected function setStrength(Strength $strength): Combatant
    {
        $this->strength = $strength;
        return $this;
    }

    public function multiplyStrength($value)
    {
        $this->strength = $this->strength->multiply($value);
    }

    /**
     * @return bool
     */
    public function isMissingNextAttack(): bool
    {
        return $this->isMissingNextAttack;
    }

    /**
     * @param bool $isMissingNextAttack
     * @return Combatant
     */
    public function setIsMissingNextAttack(bool $isMissingNextAttack = true): Combatant
    {
        $this->isMissingNextAttack = $isMissingNextAttack;
        return $this;
    }

    /**
     * @return Defense
     */
    public function defense(): Defense
    {
        return $this->defense;
    }

    /**
     * @param Defense $defense
     * @return Combatant
     */
    protected function setDefense(Defense $defense): Combatant
    {
        $this->defense = $defense;
        return $this;
    }

    /**
     * @return Speed
     */
    public function speed(): Speed
    {
        return $this->speed;
    }

    /**
     * @param Speed $speed
     * @return Combatant
     */
    protected function setSpeed(Speed $speed): Combatant
    {
        $this->speed = $speed;
        return $this;
    }

    /**
     * @return Luck
     */
    public function luck(): Luck
    {
        return $this->luck;
    }

    /**
     * @param Luck $luck
     * @return Combatant
     */
    protected function setLuck(Luck $luck): Combatant
    {
        $this->luck = $luck;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDefender(): bool
    {
        return $this->isDefender;
    }

    public function setIsDefender(bool $isDefender = true): Combatant
    {
        $this->isDefender = $isDefender;
        return $this;
    }

    public function setIsAttacker(bool $isAttacker = true): Combatant
    {
        $this->isDefender = !$isAttacker;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAttacker(): bool
    {
        return !$this->isDefender;
    }

    /**
     * @return bool
     */
    public function hasLuckyDefense(): bool
    {
        return $this->hasLuckyDefense;
    }

    public function calcLuckyDefense()
    {
        if ($this->luck->isRandInInterval()) {
            $this->log($this->name . ' defended successfully');
            $this->hasLuckyDefense = true;
        } else {
            $this->hasLuckyDefense = false;
        }

    }

    private function log($message)
    {
        if (!$this->logger) {
            $this->logger = Logger::instance();
        }
        $this->logger->log($message);
    }

    public function reset()
    {
        $this->hasLuckyDefense = false;
        $this->isMissingNextAttack = false;
    }

    public function implementSkillsWith(Combatant $combatant)
    {
        foreach ($this->skills as $skill) {
            $skill->execute($this, $combatant);
        }
    }

    protected function includeToString()
    {
        return [
            'name',
            'health',
            'strength',
            'defense',
            'strength',
            'speed',
            'luck',
        ];
    }


}