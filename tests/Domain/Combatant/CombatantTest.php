<?php
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 21.04.18
 * Time: 23:44
 */

namespace Combat\Domain\Combatant;


use Combat\Domain\Combatant\CombatantTest\CombatantConcrete;
use Combat\Domain\Combatant\Values\Health;
use Combat\Domain\Exceptions\InvalidRangeObjectValueException;
use PHPUnit\Framework\TestCase;

class CombatantTest extends TestCase
{
    public function testGetRandomAllowedValue()
    {
        $combatant = $this->combatant(['health' => [1, 10]]);
        $this->assertInstanceOf(Health::class, $combatant->health());
    }


    public function testInvalidRangeThrowException()
    {
        $this->expectException(InvalidRangeObjectValueException::class);
        $this->combatant(['health' => [10, 0]]);
    }

    public function testGetFloatRandomAllowedValue()
    {
        $combatant = $this->combatant(['luck' => [0.1, 0.5]]);
        $value = $combatant->luck()->value();
        $this->assertTrue(is_float($value));
        $this->assertTrue($value >= 0.1);
        $this->assertTrue($value <= 0.5);
    }


    private function combatant($values = []): CombatantConcrete
    {
        return new CombatantConcrete('bar', $values);
    }

}

namespace Combat\Domain\Combatant\CombatantTest;

use Combat\Domain\Combatant\Combatant;

class CombatantConcrete extends Combatant
{
    public $allowedValues = [];

    public function __construct($name, array $allowedValues)
    {
        $this->allowedValues = $allowedValues;
        parent::__construct($name);
    }


    public function withAllowedValues(array $values)
    {
        $this->allowedValues = $values;
        return $this;
    }

    protected function allowedValues(): array
    {
        return $this->allowedValues;
    }


}

class CombatantConcrete2 extends Combatant
{

    public function getRandomAllowedValue($name)
    {
        if ($name == 'health') {
            return 2;
        }
        return 10;
    }

    protected function allowedValues(): array
    {
        return [
            'health' => [],
            'strength' => [],
        ];
    }

}