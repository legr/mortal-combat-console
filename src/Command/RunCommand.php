<?php declare(strict_types=1);

/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 21.04.18
 * Time: 22:02
 */

namespace Combat\Command;

use Combat\Domain\Battle;
use Combat\Domain\Combatant\Factory as CombatantFactory;
use Combat\Domain\Logger;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class RunCommand extends Command
{
    /**
     * @var CombatantFactory
     */
    protected $combatantFactory;

    /**
     * @var Logger
     */
    protected $logger;

    protected function configure()
    {
        $this->setName('run');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('');
        $output->writeln('**********************************************');
        $output->writeln('************  MORTAL COMBAT CONSOLE **********');
        $output->writeln('**********************************************');
        $output->writeln('');

        $helper = $this->getHelper('question');

        $question = new Question('Please enter the name of the first combatant (Hector): ', 'Hector');
        $firstName = $helper->ask($input, $output, $question);

        $question = new Question('Please enter the name of the second combatant (Achilles):', 'Achilles');
        $secondName = $helper->ask($input, $output, $question);

        $combatant1 = $this->combatantFactory->randomWithName($firstName);
        $combatant2 = $this->combatantFactory->randomWithName($secondName);

        $battle = new Battle($combatant1, $combatant2);
        $battle->run();
        $output->writeln($this->logger->messages());

    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->combatantFactory = new CombatantFactory();
        $this->logger = Logger::instance();
    }
}