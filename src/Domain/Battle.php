<?php declare(strict_types=1);
/**
 * Created by PhpStorm.
 * Author: Oleg Chepkin <oc@mobioinc.com>
 * Date: 22.04.18
 * Time: 19:44
 */

namespace Combat\Domain;


use Combat\Domain\Combatant\Combatant;

class Battle
{
    use LoggerTrait;

    const MaxIteration = 30;
    /**
     * @var Combatant
     */
    protected $comb1;

    /**
     * @var Combatant
     */
    protected $comb2;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var Combatant
     */
    protected $winner;

    protected $firstIteration = true;

    public function __construct(Combatant $comb1, Combatant $comb2)
    {
        $this->comb1 = $comb1;
        $this->comb2 = $comb2;
    }

    public function run()
    {
        $this->log([$this->comb1, $this->comb2]);
        $this->log('Start Battle');
        $this->scoreboard();
        for ($i = 1; $i <= self::MaxIteration; $i++) {
            $this->log(['Attack #' . $i, '--------------']);
            $this->choseAttacker();
            $this->calcLuck();
            $this->implementSkills();
            $this->calcResult();
            if ($this->wasSomeoneDefeated()) {
                break;
            }

            $this->log('=================================');
        }
        if (!$this->winner) {
            $this->draw();
        }
        $this->logger->log('Game Over');

    }

    protected function scoreboard(): void
    {
        $this->log([
            '',
            '*************************',
            $this->comb1->scoreboard(),
            $this->comb2->scoreboard(),
            '*************************',
            '',
        ]);
    }

    protected function choseAttacker()
    {

        $this->choseAttackerFirstIteration();
        $this->choseAttackerNextIterations();

        $this->log($this->attacker()->name() . ' attack...');

        $this->comb1->reset();
        $this->comb2->reset();
    }

    protected function calcLuck()
    {
        $this->defender()->calcLuckyDefense();
    }

    protected function implementSkills()
    {
        $defender = $this->defender();
        $attacker = $this->attacker();
        $attacker->implementSkillsWith($defender);
        $defender->implementSkillsWith($attacker);

    }

    protected function calcResult()
    {
        $defender = $this->defender();
        if (!$defender->hasLuckyDefense()) {
            $damage = $defender->damage($this->attacker()->strength());
            $this->log($defender->name() . ' is dealt ' . $damage . ' damage');
        }
        $this->scoreboard();
    }

    private function wasSomeoneDefeated()
    {
        $comb1 = $this->comb1;
        $comb2 = $this->comb2;
        $winner = null;
        if ($comb1->health()->isZero() && $comb2->health()->isZero()) {
            $this->draw();
            return true;
        }
        if ($comb1->health()->isZero()) {
            $winner = $comb2;
        }
        if ($comb2->health()->isZero()) {
            $winner = $comb1;
        }
        if ($winner) {
            $this->winner = $winner;
            $this->log($winner->name() . ' the winner');
            return true;
        }
        return false;
    }

    private function draw()
    {
        $this->log('Draw');
    }

    private function choseAttackerFirstIteration(): void
    {
        if ($this->firstIteration) {
            $comb1 = $this->comb1;
            $comb2 = $this->comb2;
            $speed1 = $comb1->speed()->value();
            $speed2 = $comb2->speed()->value();
            if ($speed1 > $speed2) {
                $comb2->setIsDefender();
            } else if ($speed1 = $speed2) {
                if ($comb1->defense() < $comb2->defense()) {
                    $comb2->setIsDefender();
                } else {
                    $comb1->setIsDefender();
                }
            } else {
                $comb2->setIsDefender();
            }
            $this->firstIteration = false;
        }

    }

    private function choseAttackerNextIterations()
    {
        if (!$this->firstIteration) {

            $comb1 = $this->comb1;
            $comb2 = $this->comb2;
            if (!$comb1->isMissingNextAttack() && !$comb2->isMissingNextAttack()) {

                if ($comb2->isAttacker()) {
                    $comb1->setIsAttacker();
                    $comb2->setIsDefender();
                } else {
                    $comb1->setIsDefender();
                    $comb2->setIsAttacker();
                }
            }
        }
    }

    private function attacker()
    {
        $comb1 = $this->comb1;
        if ($comb1->isAttacker()) {
            return $comb1;
        }
        return $this->comb2;
    }

    private function defender()
    {
        $comb1 = $this->comb1;
        if ($comb1->isDefender()) {
            return $comb1;
        }
        return $this->comb2;
    }

}